import java.util.Scanner;

public class VirtualPetApp {

	public static void main(String[] args) {
	
	Scanner reader = new Scanner(System.in);
	
	Bunny[] fluffle = new Bunny[3];
	
	for(int i = 0; i < fluffle.length; i++) {
		
		System.out.println("Enter the bunnys colour");
		String colour = reader.nextLine();
		
		System.out.println("Enter the bunnys type");
		String type = reader.nextLine();
		
		System.out.println("Enter the bunnys speed");
		int speed = Integer.parseInt(reader.nextLine());
		
		fluffle[i] = new Bunny(colour, type, speed);
		
		System.out.println("");
	}
	System.out.println("This is the first print of the status of your last Bunny");
	System.out.println(fluffle[fluffle.length-1].getColour());
	System.out.println(fluffle[fluffle.length-1].getType());
	System.out.println(fluffle[fluffle.length-1].getSpeed());
	System.out.println("Please enter a new colour for the last bunny");
	String newColour = reader.nextLine();
	fluffle[fluffle.length-1].setColour(newColour);
	System.out.println("This is the print after you changed the colour of the status of your last Bunny");
	System.out.println(fluffle[fluffle.length-1].getColour());
	System.out.println(fluffle[fluffle.length-1].getType());
	System.out.println(fluffle[fluffle.length-1].getSpeed());
	}
}