public class Bunny {
	private String colour;
	private String type;
	private int speed;
	
	public Bunny(String colour, String type, int speed){
		this.colour = colour;
		this.type = type;
		this.speed = speed;
	}
	
	public void setColour(String colour){
		this.colour = colour;
	}
	
	public String getColour(){
		return this.colour;
	}
	
	public String getType(){
		return this.type;
	}
	
	public int getSpeed(){
		return this.speed;
	}
	
	public void sleep() {
		System.out.println("Sleeping! zZz");
		
	}
	
	public void eatCarrot() {
		if(this.colour == "white"){
			System.out.println("So hungry... :(");
		}
		else {
			System.out.println("Eating a carrot!");
		}
	}
}